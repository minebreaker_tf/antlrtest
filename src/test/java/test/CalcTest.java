package test;


import org.antlr.v4.runtime.RecognitionException;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class CalcTest {

    @Test
    public void test1() {
        double res = Main.calculate("1 + 2");
        assertThat(res, is(3.0));
    }

    @Test
    public void test2() {
        double res = Main.calculate("3.5 - 2");
        assertThat(res, is(1.5));
    }

    @Test
    public void test3() {
        double res = Main.calculate("3 * 1.5");
        assertThat(res, is(4.5));
    }

    @Test
    public void test4() {
        double res = Main.calculate("1 + 2 * 3");
        assertThat(res, is(7.0));
    }

    @Test
    public void test5() {
        double res = Main.calculate("1 * 2 + 3");
        assertThat(res, is(5.0));
    }

    @Test
    public void test6() {
        double res = Main.calculate("(100 + 200) / 3");
        assertThat(res, is(100.0));
    }

    @Test
    public void test7() {
        double res = Main.calculate("(8 * (3 - 1)) / 2 - (7 + 5) / (-9 + 12)");
        assertThat(res, is(4.0));
    }

    @Test
    public void test8() {
        double res = Main.calculate("-5 + 2");
        assertThat(res, is(-3.0));
    }

    @Test(expected = RecognitionException.class)
    public void testException() {
        Main.calculate("illegal expression");
    }

}

package test;

import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class Slf4jErrorListener extends BaseErrorListener {

    private static final Logger logger = LoggerFactory.getLogger(Slf4jErrorListener.class);

    private static final Slf4jErrorListener instance = new Slf4jErrorListener();

    private Slf4jErrorListener() {
    }

    public static Slf4jErrorListener getInstance() {
        return instance;
    }

    @Override
    public void syntaxError(
            Recognizer<?, ?> recognizer,
            Object offendingSymbol,
            int line,
            int charPositionInLine,
            String msg,
            RecognitionException e) {
        logger.error("line {}:{} {}", line, charPositionInLine, msg);
    }

}

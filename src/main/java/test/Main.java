package test;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.BufferedTokenStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class Main {

    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {

        if (args.length != 1) return;

        double result = calculate(args[0]);

        logger.info("Result: {}", result);
    }

    public static double calculate(String expr) {
        CalcLexer lexer = new CalcLexer(new ANTLRInputStream(expr));
        CalcParser parser = new CalcParser(new BufferedTokenStream(lexer));

        CalcParser.ExpressionContext ctx = parser.expression();
        double res = new Visitor().visit(ctx);

        logger.info("Result: {}", res);

        return res;
    }

}

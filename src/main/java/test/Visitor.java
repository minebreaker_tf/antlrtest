package test;

import org.antlr.v4.runtime.tree.TerminalNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class Visitor {

    private static final Logger logger = LoggerFactory.getLogger(Visitor.class);

    public double visit(CalcParser.ExpressionContext ctx) {

        logger.debug("{}", ctx.getText());

        if (ctx.exception != null) {
            logger.error("Recognition error", ctx.exception);
            throw ctx.exception;
        }

        if (ctx.NUMBER() != null) {
            return Double.parseDouble(ctx.getText());
        } else if (ctx.expression().size() == 1) {
            return visit(ctx.expression().get(0));
        }

        TerminalNode op = ctx.operator() != null ?
                (TerminalNode) ctx.operator().getChild(0) :
                (TerminalNode) ctx.primaryOperator().getChild(0);

        double x = visit(ctx.expression().get(0));
        double y = visit(ctx.expression().get(1));
        switch (op.getSymbol().getType()) {
        case CalcParser.PLUS:
            return x + y;
        case CalcParser.MINUS:
            return x - y;
        case CalcParser.MULTIPLY:
            return x * y;
        case CalcParser.DIVIDE:
            return x / y;
        default:
            throw new IllegalStateException();
        }
    }

}

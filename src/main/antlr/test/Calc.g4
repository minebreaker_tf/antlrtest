grammar Calc;

@header {
package test;
}

// 初期化子を使い、パーサー/レクサーのインスタンス作成時にリスナーをセットする。
// http://stackoverflow.com/questions/11194458/forcing-antlr-to-use-my-custom-treeadaptor-in-a-parser
@members {
{
    this.removeErrorListeners();
    this.addErrorListener(Slf4jErrorListener.getInstance());
}
}

NEWLINE
    : '\r'? '\n' -> skip
    ;

WS
    : (' ' | '\t') -> skip
    ;

PLUS
    : '+'
    ;

MINUS
    : '-'
    ;

MULTIPLY
    : '*'
    ;

DIVIDE
    : '/'
    ;

NUMBER
    : '-'? [0-9]+ ('.' [0-9])?
    ;

expression
    : '(' expression ')'
    | expression primaryOperator expression
    | expression operator expression
    | NUMBER
    ;

operator
    : PLUS
    | MINUS
    ;

primaryOperator
    : MULTIPLY
    | DIVIDE
    ;
